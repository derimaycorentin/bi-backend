import { Component, OnInit } from '@angular/core';
import Chart, { BubbleController, ChartDataCustomTypesPerDataset, ChartOptions, ChartTypeRegistry } from 'chart.js/auto';
import ChartLabels from 'chartjs-plugin-datalabels';
import service from 'src/app/services/service';


declare function setMapJS(CAPays:any, nbClient:any):any;
declare function setPipelineJS(data:any, CA:any):any;
declare function getPropertyValueJS(data:any):any;

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
	
	chartCA: any;
	chartChallenge: any;
	chartCamembert:any;
	chartTauxConversion: any;
	chartCAExport: any;

	commerciaux:any;
	annee:number = localStorage['annee'] !== undefined ? localStorage['annee'] : 2017;
	table:any = [];
	selectedCommercial:any/* = localStorage.getItem('comm') !== null ? JSON.parse(localStorage.getItem('comm')) : undefined*/;
	selectedCommercialName:string | undefined;
	selectedCommercialObjectif:number | undefined;
	
	GlobalCA:string | undefined;
	GlobalCAPipeline:number | undefined;
	GlobalCANM1:string | undefined;
	
	CAFrance:string | undefined;
	CAFranceNM1:string | undefined;
	CAPotentiel:string | undefined;
	PositiveCA:boolean | undefined;
	PositiveCAFR:boolean | undefined;
	CamembertGotContent:boolean = true;
	ObjectifRate:string | undefined;

	CAInternational:string | undefined;
	CAInternationalNM1:string | undefined;
	PositiveCAInter:boolean | undefined;

	ngOnInit() {
		Chart.register(ChartLabels);
		this.getCommerciaux({"target":{"value":this.setTargetCommercial()}});
	}

	setTargetCommercial() {
		let value, result = 0;
		value = localStorage.getItem('comm');
		if (value !== 'undefined' && value !== null){
			value = JSON.parse(value !== null ? value : '')
			this.selectedCommercial = value;
			result = value.IDCommercial;
        }
		return result;
	}

	async getCommerciaux(refreshTarget:any) {
		const response = await service.getCommerciaux();
		const data = await response.json();
		this.commerciaux = data[0];
		await this.commerciaux.unshift({"IDCommercial": 0,"NomCommercial": "Tous","Objectif": 0});
		await refreshTarget.target.value !== undefined ? this.updateCommercial(refreshTarget) : this.updateCommercial();
		await this.commerciaux.splice(1,1);
	}

	async updateDashboard() {
		this.selectedCommercialName = this.selectedCommercial.NomCommercial;
		this.selectedCommercialObjectif = this.selectedCommercial.IDCommercial > 0 ? this.selectedCommercial.Objectif : 0;
		if (this.selectedCommercialObjectif === 0) {
			let objectif = 0;
			this.commerciaux.forEach((element:any) => {
				objectif += element.Objectif;
			});;
			this.selectedCommercialObjectif = objectif;
		}
		await this.getGlobalCA(this.annee, this.selectedCommercial.IDCommercial || null);
		await this.getCAForPipeline(this.selectedCommercial.IDCommercial || null);
		this.getFranceCA(this.annee, this.selectedCommercial.IDCommercial || null);
		this.getInternationalCA(this.annee, this.selectedCommercial.IDCommercial || null);
		this.setPipeline(this.selectedCommercial.IDCommercial || null);
		this.updateImportExport(this.annee, this.selectedCommercial.IDCommercial || null);
		this.updateTauxConversion(this.annee, this.selectedCommercial.IDCommercial || null);
		this.updateCAAnnee(this.annee, this.selectedCommercial.IDCommercial || null);
		this.setTable(this.annee, this.selectedCommercial.IDCommercial || null);
		this.updateCamembert(this.annee, this.selectedCommercial.IDCommercial || null);
		this.updateChallenge(this.annee, this.selectedCommercial.IDCommercial || null);
		this.setMap(this.selectedCommercial.IDCommercial || null, this.annee);
	}

	updateCommercial(event:any = {"target":{"value":0}}) {
		this.selectedCommercial = this.commerciaux.filter((c:any) => { return c.IDCommercial == event.target.value})[0];
		localStorage.setItem("comm", JSON.stringify(this.selectedCommercial));
		this.updateDashboard();
	}

	updateAnnee(event:any = {"target":{"value":0}}) {
		this.annee = event.target.value;
		localStorage['annee'] = this.annee;
		this.updateDashboard();
	}

	isPositive(a:number | undefined,b:number | undefined):boolean {
		return (a !== undefined ? a : 0) > (b !== undefined ? b : 0);
	}

	async getCAForPipeline(IDCommercial:number) {
		const response = await service.getCADate(null,2017,IDCommercial);
		const data = await response.json();

		let CA = 0;

		data[0].forEach((element: any) => {
			CA += element.Montant;
		});
		this.GlobalCAPipeline = CA;
	}

	async getGlobalCA(Annee:number, IDCommercial:number) {
		const response = await service.getCADate(null,Annee,IDCommercial);
		const data = await response.json();

		const responseNM1 = await service.getCADate(null,Annee-1,IDCommercial);
		const dataNM1 = await responseNM1.json();

		let CA = 0;
		let CANM1 = 0;

		data[0].forEach((element: any) => {
			CA += element.Montant;
		});
		dataNM1[0].forEach((element: any) => {
			CANM1 += element.Montant;
		});
		this.ObjectifRate = new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 })
			.format(CA * 100 / (this.selectedCommercialObjectif as number));
		this.PositiveCA = CA > CANM1;
		this.GlobalCA = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(CA);
		this.GlobalCANM1 = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(CANM1);
	}

	async getFranceCA(Annee:number, IDCommercial:number) {
		const response = await service.getCAPays(null,IDCommercial,1,Annee);
		const data = await response.json();

		const responseNM1 = await service.getCAPays(null,IDCommercial,1,Annee-1);
		const dataNM1 = await responseNM1.json();

		let CA = 0;
		let CANM1 = 0;

		data[0].forEach((element: any) => {
			CA += element.Montant;
		});
		dataNM1[0].forEach((element: any) => {
			CANM1 += element.Montant;
		});
		this.PositiveCAFR = CA > CANM1;
		this.CAFrance = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(CA);
		this.CAFranceNM1 = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(CANM1);
	}

	async getInternationalCA(Annee:number, IDCommercial:number) {
		const response = await service.getCAPays(null,IDCommercial,0,Annee);
		const data = await response.json();

		const responseNM1 = await service.getCAPays(null,IDCommercial,0,Annee-1);
		const dataNM1 = await responseNM1.json();

		let CA = 0;
		let CANM1 = 0;

		data[0].forEach((element: any) => {
			CA += element.Montant;
		});
		dataNM1[0].forEach((element: any) => {
			CANM1 += element.Montant;
		});
		this.PositiveCAInter = CA > CANM1;
		this.CAInternational = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(CA);
		this.CAInternationalNM1 = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(CANM1);
	}

	async setMap(IDCommercial:number, annee:number) {
		const response = await service.getCAPays(null,IDCommercial,null, annee);
		const responseClient = await service.getCAPays(null,null,null,null);
		const data = await response.json();
		const dataClient = await responseClient.json();

		let CAPays:any = {};
		let nbClient:any = {};
		data[0].forEach((element: any) => {
			CAPays[element.Code] = element.Montant+' €';
		});
		dataClient[0].forEach((element: any) => {
			nbClient[element.Code] = element.NbClient;
		});
		setMapJS(CAPays, nbClient);
	}

	async setPipeline(IDCommercial:number) {
		let Annee:number = 2017;
		const response = await service.getPipeline(Annee,IDCommercial);
		const data = await response.json();
		const potentiel = (data[0][0]['0-30%']*0.2) 
						+ (data[0][0]['31-60%']*0.4) 
						+ (data[0][0]['61-80%']*0.6) 
						+ (data[0][0]['>81%']*0.7)
						+ (this.GlobalCAPipeline === undefined ? 0 : this.GlobalCAPipeline);
		setPipelineJS(data[0][0], this.GlobalCAPipeline);
		this.CAPotentiel = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(potentiel);
	}

	async setTable(Annee:number, IDCommercial:number) {
		const response = await service.getByMotif(Annee,IDCommercial);
		const data = await response.json();
		
		this.table = data[0];	
	}

	async updateImportExport(Annee:number, IDCommercial:number) {
		const responseFrance = await service.getCAPays(null,IDCommercial,1,Annee);
		const responseFranceNM1 = await service.getCAPays(null,IDCommercial,1,Annee-1);
		const responseInternational = await service.getCAPays(null,IDCommercial,0,Annee);
		const responseInternationalNM1 = await service.getCAPays(null,IDCommercial,0,Annee-1);

		const dataFrance = await responseFrance.json();
		const dataFranceNM1 = await responseFranceNM1.json();
		const dataInternational = await responseInternational.json();
		const dataInternationalNM1 = await responseInternationalNM1.json();

		let Import:any = {};
		let tmpImport:number = 0;
		let ImportNM1:any = {};
		let tmpImportNM1:number = 0;
		let Export:any = {};
		let tmpExport:number = 0;
		let ExportNM1:any = {};
		let tmpExportNM1:number = 0;
		
		dataFrance[0].forEach((element: any) => {
			tmpImport += element.Montant === null ? 0 : element.Montant;
			// Import[element.Année] = Import[element.Année] !== undefined ? Import[element.Année] + element.Montant === null ? 0 : element.Montant : element.Montant === null ? 0 : element.Montant;
		});
		Import[Annee] = tmpImport;
		dataFranceNM1[0].forEach((element: any) => {
			tmpImportNM1 += element.Montant === null ? 0 : element.Montant;
			// ImportNM1[element.Année] = ImportNM1[element.Année] !== undefined ? ImportNM1[element.Année] + element.Montant === null ? 0 : element.Montant : element.Montant === null ? 0 : element.Montant;
		});
		ImportNM1[Annee-1] = tmpImportNM1;
		dataInternational[0].forEach((element: any) => {
			tmpExport += element.Montant === null ? 0 : element.Montant;
			// Export[element.Année] = Export[element.Année] !== undefined ? Export[element.Année] + element.Montant === null ? 0 : element.Montant : element.Montant === null ? 0 : element.Montant;
		});
		Export[Annee] = tmpExport;
		dataInternationalNM1[0].forEach((element: any) => {
			tmpExportNM1 += element.Montant === null ? 0 : element.Montant;
			// ExportNM1[element.Année] = ExportNM1[element.Année] !== undefined ? ExportNM1[element.Année] + element.Montant === null ? 0 : element.Montant : element.Montant === null ? 0 : element.Montant;
		});
		ExportNM1[Annee-1] = tmpExportNM1;

		const data = {
			datasets: [
				{
					label: 'Import',
					data: Import,
					borderColor: getPropertyValueJS('--primary'),
					backgroundColor: getPropertyValueJS('--primary'),
				},
				{
					label: 'Export',
					data: Export,
					borderColor: getPropertyValueJS('--secondary'),
					backgroundColor: getPropertyValueJS('--secondary'),
				},
				{
					label: 'Import (n-1)',
					data: ImportNM1 ,
					borderColor: getPropertyValueJS('--primary'),
					backgroundColor: getPropertyValueJS('--primary'),
				},
				{
					label: 'Export (n-1)',
					data: ExportNM1 ,
					borderColor: getPropertyValueJS('--secondary'),
					backgroundColor: getPropertyValueJS('--secondary'),
				}
			]
		};

		if (this.chartCAExport !== undefined) {
			this.chartCAExport.destroy();
		}

		this.chartCAExport = new Chart("ca-origin", {
			type: 'bar',
			data: data,
			options: {
				responsive: true,
				maintainAspectRatio: false,
				plugins: {
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'CA Import & Export | N vs N-1 (€)',
						color:getPropertyValueJS('--primary-dark'),
						font: {
							family:'Poppins',
							size:16
						}
					}
				}
			},
		});
		this.chartCAExport.update(); 
	}

	async updateTauxConversion(Annee:number, IDCommercial:number) {
		
		const response = await service.getTauxConversion(Annee,IDCommercial);

		const dataTaux = await response.json();

		let NombreCommande:any = {};
		let NombreOffre:any = {};
		let month:any = [];
		let chartType:any;
		
		dataTaux[0].forEach((element: any) => {
			month.indexOf(element.Mois) === -1 ? month.push(element.Mois) : null;
			NombreCommande[element.Mois] = NombreCommande[element.Mois] !== undefined ? NombreCommande[element.Mois] + (element.NombreCommande === null ? 0 : element.NombreCommande) : element.NombreCommande === null ? 0 : element.NombreCommande;
			NombreOffre[element.Mois] = NombreOffre[element.Mois] !== undefined ? NombreOffre[element.Mois] + (element.NombreOffre === null ? 0 : element.NombreOffre) : element.NombreOffre === null ? 0 : element.NombreOffre;
		});

		chartType = month.length > 1 ? 'line' : 'bar';

		const data = {
			datasets: [
				{
					label: 'Nombre de commandes',
					data: NombreCommande,
					borderColor: getPropertyValueJS('--primary'),
					backgroundColor: getPropertyValueJS('--primary'),
				},
				{
					label: "Nombre d'offres",
					data: NombreOffre,
					borderColor: getPropertyValueJS('--secondary'),
					backgroundColor: getPropertyValueJS('--secondary'),
				}
			]
		};

		if (this.chartTauxConversion !== undefined) {
			this.chartTauxConversion.destroy();
		}

		this.chartTauxConversion = new Chart("taux-conversion", {
			type: chartType,
			data: data,
			options: {
				responsive: true,
				maintainAspectRatio: false,
				plugins: {
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Évolution offres & commandes / mois',
						color:getPropertyValueJS('--primary-dark'),
						font: {
							family:'Poppins',
							size:16
						}
					}
				}
			},
		});
		
		this.chartTauxConversion.update(); 
	}

	async updateCAAnnee(Annee:number, IDCommercial:number) {
		
		const response = await service.getCADate(null,Annee,IDCommercial);
		const responseNM1 = await service.getCADate(null,Annee-1,IDCommercial);

		const dataCA = await response.json();
		const dataNM1 = await responseNM1.json();

		let CA:any = {};
		let tempCA:number = 0;
		let CAnm1:any = {};
		let tempCAnm1:number = 0;
		const date = new Date();

		for(let i = 1 ; i<13 ; i++) {
			let tmp = 0;
			date.setMonth(i - 1);
			dataCA[0].filter(function(x:any){return x.Mois === i}).forEach((element:any) => {
				tmp += element.Montant;
			});
			tempCA += tmp === undefined ? 0 : tmp;
			let month =  date.toLocaleString('fr-FR', { month: 'long' })
			CA[month.charAt(0).toUpperCase() + month.slice(1).toLowerCase()] = tempCA;
		}

		for(let i = 1 ; i<13 ; i++) {
			let tmp = 0;
			date.setMonth(i - 1);
			dataNM1[0].filter(function(x:any){return x.Mois === i}).forEach((element:any) => {
				tmp += element.Montant;
			});
			tempCAnm1 += tmp === undefined ? 0 : tmp;
			let month =  date.toLocaleString('fr-FR', { month: 'long' })
			CAnm1[month.charAt(0).toUpperCase() + month.slice(1).toLowerCase()] = tempCAnm1;
		}

		
		// date.setMonth(monthNumber - 1);
		// let monthName = date.toLocaleString('fr-FR', { month: 'long' });

		const data = {
			datasets: [
				{
					label: 'CA',
					data: CA,
					borderColor: getPropertyValueJS('--primary'),
					backgroundColor: getPropertyValueJS('--primary'),
				},
				{
					label: 'CA n-1',
					data: CAnm1,
					borderColor: getPropertyValueJS('--secondary'),
					backgroundColor: getPropertyValueJS('--secondary'),
				},
				{
					label: 'Objectif',
					data: {
						'Janvier':this.selectedCommercialObjectif,
						'Février':this.selectedCommercialObjectif,
						'Mars':this.selectedCommercialObjectif,
						'Avril':this.selectedCommercialObjectif,
						'Mai':this.selectedCommercialObjectif,
						'Juin':this.selectedCommercialObjectif,
						'Juillet':this.selectedCommercialObjectif,
						'Août':this.selectedCommercialObjectif,
						'Septembre':this.selectedCommercialObjectif,
						'Octobre':this.selectedCommercialObjectif,
						'Novembre':this.selectedCommercialObjectif,
						'Décembre':this.selectedCommercialObjectif
					},
					borderColor: getPropertyValueJS('--secondary-dark'),
					backgroundColor: getPropertyValueJS('--secondary-dark'),
				}
			]
		};

		if (this.chartCA !== undefined) {
			this.chartCA.destroy();
		}

		this.chartCA = new Chart("ca-chart", {
			type: 'line',
			data: data,
			options: {
				responsive: true,
				maintainAspectRatio: false,
				plugins: {
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'CA cumulé & Objectif | N vs N-1 (€)',
						color:getPropertyValueJS('--primary-dark'),
						font: {
							family:'Poppins',
							size:16
						}
					}
				}
			},
		});
		
		this.chartCA.update(); 
	}

	async updateCamembert(Annee:number, IDCommercial:number) {

		const response = await service.getByMotif(Annee,IDCommercial);
		const dataJson = await response.json();
		
		let sansSuite:any = 0;
		let perdu:any = 0;
		let commande:any = 0;

		dataJson[0].forEach((element:any) => {
			sansSuite += element.NbOffreSS;
			perdu += element.NbOffrePerdu;
			commande += element.NbOffreCde;
		});
		
		const data = {
			labels: ['Sans suite','Perdu','Commandé'],
			datasets: [{
				data:[sansSuite,perdu,commande]
				,backgroundColor: [getPropertyValueJS('--secondary-dark')
				,getPropertyValueJS('--primary')
				,getPropertyValueJS('--secondary')]
			}]
		};

		if (this.chartCamembert !== undefined) {
			this.chartCamembert.destroy();
		}
		
		this.chartCamembert = new Chart("pie", {
			type: 'doughnut',
			data: data,
			options: {
				responsive: true,
				maintainAspectRatio: false,
				plugins: {
					legend: {
						position: 'top',
					},
					datalabels: {
						borderRadius:16,
						color: getPropertyValueJS('--primary-dark'),
						display: 'auto',
						font: {
							family:'Poppins',
							size:12,
							weight:'bold'
						},
						anchor:'start',
						formatter: (value, ctx) => {
							const dataPoints =  ctx.chart.data.datasets[0].data;
							let totalSum = function (total:any, point:any) {
								return total + point;
							}
							const totalValue = dataPoints.reduce(totalSum,0);
							const percentageValue = (value / totalValue * 100).toFixed(1);
							const result = [`Offres : ${value}`, `Part : ${percentageValue}%`]
							if (value > 0) {
								return result;
							}
							
							return '';
						},
					},
					title: {
						display: true,
						text: 'Issue des offres',
						color:getPropertyValueJS('--primary-dark'),
						font: {
							family:'Poppins',
							size:16
						}
					}
				}
			},
		});

		if (sansSuite+perdu+commande === 0) {
			this.CamembertGotContent = false;
		}
		else {
			this.CamembertGotContent = true;
		}

		this.chartCamembert.update(); 
	}

	async updateChallenge(Annee:number, IDCommercial:number) {

		const response = await service.getCADate(null,Annee,IDCommercial);
		const dataJson = await response.json();
		
		let ca:any = {};
		let tmpComm:any = [];
		let objectif:any = {};

		this.commerciaux.forEach((element:any) => {
			tmpComm.push(element);
		});
		dataJson[0].forEach((element:any) => {
			ca[element.NomCommercial] = ca[element.NomCommercial] !== undefined ?  ca[element.NomCommercial] + element.Montant : element.Montant;
		});
		
		tmpComm.shift()
		tmpComm.forEach((element:any) => {
			if (this.selectedCommercial?.IDCommercial === 0)
				objectif[element.NomCommercial] = element.Objectif;
			else
				if (this.selectedCommercial?.IDCommercial === element.IDCommercial)
					objectif[element.NomCommercial] = element.Objectif;
		});

		let ObjectifType:keyof ChartTypeRegistry = Object.keys(objectif).length > 1 ? 'line' : 'bar';
		
		const data:ChartDataCustomTypesPerDataset = {
			datasets: [{
				label:'Objectif'
				,data:objectif
				,borderColor: getPropertyValueJS('--secondary')
				,backgroundColor: getPropertyValueJS('--secondary')
				,type:ObjectifType
				,stack:'combined'
				,order:1
			},
			{
				label:'Chiffre d\'affaires'
				,data:ca
				,borderColor: getPropertyValueJS('--secondary-dark')
				,backgroundColor: getPropertyValueJS('--secondary-dark')
				,type:'bar'
				,order:2
			}]
		};

		if (this.chartChallenge !== undefined) {
			this.chartChallenge.destroy();
		}
		
		
		this.chartChallenge = new Chart("challenge", {
			type: 'line',
			data: data,
			options: {
				responsive: true,
				maintainAspectRatio: false,
				plugins: {
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'CA réalisé VS Objectif de CA (€)',
						color:getPropertyValueJS('--primary-dark'),
						font: {
							family:'Poppins',
							size:16
						}
					}
				},
				scales: {
					y: {
						stacked: true
					}
				}
			}
		});

		this.chartChallenge.update(); 
	}

	parseAmount(data:any) {
		return new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(data);
	}

}