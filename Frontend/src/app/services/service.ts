
const baseUrl = "http://localhost:4000/kpi";

export default {

    async getCommerciaux() {
        return await fetch(new URL(`${baseUrl}/commerciaux`));
    },
    
    async getCADate(mois:number | null , annee:number | null, IDCommercial:number | null ) {
        let request = `ca-date?`;
        request += mois === null || undefined ? '' : `mois=${mois}&`;
        request += annee === null || undefined ? '' : `annee=${annee}&`;
        request += IDCommercial === null || undefined ? '' : `comm=${IDCommercial}&`;
        return await fetch(new URL(`${baseUrl}/${request}`));
    },

    async getCAPays(codePays:string | null, IDCommercial:number | null, FR:number | null, annee:number | null) {
        let request = `ca-pays?`;
        request += codePays === null || undefined ? '' : `mois=${codePays}&`;
        request += IDCommercial === null || undefined ? '' : `comm=${IDCommercial}&`;
        request += FR === null || undefined ? '' : `fr=${FR}&`;
        request += annee === null || undefined ? '' : `annee=${annee}&`;
        return await fetch(new URL(`${baseUrl}/${request}`));
    },

    async getPipeline(annee:number | null, IDCommercial:number | null ) {
        let request = `pipeline?`;
        request += annee === null || undefined ? '' : `annee=${annee}&`;
        request += IDCommercial === null || undefined ? '' : `comm=${IDCommercial}&`;
        return await fetch(new URL(`${baseUrl}/${request}`));
    },

    async getTauxConversion(annee:number | null, IDCommercial:number | null) {
        let request = `tauxconversion?`;
        request += annee === null || undefined ? '' : `annee=${annee}&`;
        request += IDCommercial === null || undefined ? '' : `comm=${IDCommercial}&`;
        return await fetch(new URL(`${baseUrl}/${request}`));
    },

    async getByMotif(annee:number | null, IDCommercial:number | null ) {
        let request = `motif?`;
        request += annee === null || undefined ? '' : `annee=${annee}&`;
        request += IDCommercial === null || undefined ? '' : `comm=${IDCommercial}&`;
        return await fetch(new URL(`${baseUrl}/${request}`));
    }

}