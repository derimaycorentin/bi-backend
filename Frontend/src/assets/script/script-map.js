
function setMapJS(data, dataCustomer) {
    // Map des noms de pays en français
    const regionNames = new Intl.DisplayNames(
        ['fr'], {type: 'region'}
    );

    $('#map').remove();
    $('#map-holder').append('<div id="map" style="height: 100%;width: 100%;"></div>');
    $('#map').vectorMap({
        map: 'world_mill_en',
        panOnDrag: true,
        lang:'fr-FR',
        focusOn: {
            x: 0.5,
            y: 0.5,
            scale: 1,
            animate: true
        },
        series: {
            regions: [{
                scale: [getPropertyValueJS('--secondary'), getPropertyValueJS('--secondary-dark')],
                normalizeFunction: 'polynomial',
                values: data,
                legend: {
                    horizontal: true,
                    title: "Chiffre d'affaires",
                    labelRender: function(v){
                        let valueThousands = v.toString().slice(0, -3) + 'K';
                        let valueMillions = v.toString().slice(0, -6) + 'M';
                        return v.toString().length > 6 ? valueMillions : v.toString().length > 3 ? valueThousands : v;
                    }
                },
            }],
        },
        onRegionTipShow: function(e, el, code){
            let value = data[code] === undefined ? '0 €' : data[code].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
            let customers =  dataCustomer[code] === undefined ? '' : ' pour '+ dataCustomer[code] +' clients'
            el.html(regionNames.of(code)+' (Chiffre d\'affaires - '+ value + customers +')');
        },
    });
    
};

function setPipelineJS(data, ca) {
    $('#pipeline').remove();
    $('<div id="pipeline" style="text-align:center;padding-top:20%">Aucune activité commerciale.</div>').insertBefore('#potential')
    let a = {'Offre 0-30%':data['0-30%']}
    let b = {'Offre 31-60%':data['31-60%']}
    let c = {'Offre 61-80%':data['61-80%']}
    let d = {'Offre >81%':data['>81%']}
    let e = {'CA actuel':ca}

    let funnel = new FunnelGraph({
        container: '#pipeline',
        gradientDirection: 'vertical',
        data: {
            labels: [Object.keys(a), Object.keys(b), Object.keys(c), Object.keys(d), Object.keys(e)],
            // colors: [getPropertyValueJS('--primary'), getPropertyValueJS('--primary-dark')],
            colors: [getPropertyValueJS('--secondary'), getPropertyValueJS('--secondary-dark')],
            values: [a[Object.keys(a)], b[Object.keys(b)], c[Object.keys(c)], d[Object.keys(d)], e[Object.keys(e)]]
        },
        displayPercent: false,
        direction: 'horizontal'
    });
    let total = a[Object.keys(a)] + b[Object.keys(b)] + c[Object.keys(c)] + d[Object.keys(d)] + e[Object.keys(e)]
    if (total > 0) {
        $('#pipeline').remove();
        $('<div id="pipeline" style="height:100%;width:100%;"></div>').insertBefore('#potential')
        funnel.draw();
    } 

    $(".label__value").map(function() {
        this.innerHTML = this.innerHTML.replace(/\,/g,' ') + ' €';
    })

}

function getPropertyValueJS(data) {
    return getComputedStyle(document.body).getPropertyValue(data);
}



