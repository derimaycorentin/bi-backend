﻿require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// API Routes
app.use('/kpi', require('./kpi/kpi.controller'));

// Start Server
const port = 4000;
app.listen(port, () => console.log('Server listening on port ' + port));