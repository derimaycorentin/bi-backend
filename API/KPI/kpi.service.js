﻿
const db = require('_helpers/db');

module.exports = {
    getCAPays,
    getCADate,
    getTauxConversion,
    getPipeline,
    getCommerciaux,
    getByMotif
};

async function getCAPays(comm = null, pays = null, annee = null, fr=null) {
    return await db.sequelize.query('EXEC sKPI_CA_PAYS @IDCommercial = :comm, @CodePays = :pays, @Année = :annee, @EstFrance = :fr',{
        replacements: {comm:comm, pays:pays, fr:fr, annee:annee}
    });
}

async function getCADate(comm = null, annee = null, mois = null) {
    return await db.sequelize.query('EXEC sKPI_CA_DATE @IDCommercial = :comm, @Année = :annee, @Mois = :mois',{
        replacements: {comm:comm, annee:annee, mois:mois}
    });
}

async function getTauxConversion(comm = null, annee = null, mois = null) {
    return await db.sequelize.query('EXEC sKPI_TauxConversion @IDCommercial = :comm, @Année = :annee, @Mois = :mois',{
        replacements: {comm:comm, annee:annee, mois:mois}
    });
}

async function getPipeline(comm = null, annee = null) {
    return await db.sequelize.query('EXEC sKPI_Pipeline @IDCommercial = :comm, @Annee = :annee',{
        replacements: {comm:comm, annee:annee}
    });
}

async function getByMotif(comm = null, annee = null) {
    return await db.sequelize.query('EXEC sKPI_Motif_Offre @IDCommercial = :comm, @Année = :annee',{
        replacements: {comm:comm, annee:annee}
    });
}

async function getCommerciaux() {
    return await db.sequelize.query('EXEC sCommerciaux')
}




