﻿const express = require('express');
const router = express.Router();
const kpiService = require('./kpi.service');

// Routes
router.get('/ca-pays', getCAPays);
router.get('/ca-date', getCADate);
router.get('/tauxconversion', getTauxConversion);
router.get('/pipeline', getPipeline);
router.get('/commerciaux', getCommerciaux);
router.get('/motif', getByMotif);

module.exports = router;

/*   Route functions  */
function getCAPays(req, res, next) {
    let comm = req.query.comm === undefined ? null : req.query.comm;
    let annee = req.query.annee === undefined ? null : req.query.annee;
    let pays = req.query.pays === undefined ? null : req.query.pays;
    let fr = req.query.fr === undefined ? null : req.query.fr;
    kpiService.getCAPays(comm, pays, annee, fr)
        .then(kpi => res.json(kpi))
        .catch(next);
}

function getCADate(req, res, next) {
    let comm = req.query.comm === undefined ? null : req.query.comm;
    let annee = req.query.annee === undefined ? null : req.query.annee;
    let mois = req.query.mois === undefined ? null : req.query.mois;
    kpiService.getCADate(comm, annee, mois)
        .then(kpi => res.json(kpi))
        .catch(next);
}

function getTauxConversion(req, res, next) {
    let comm = req.query.comm === undefined ? null : req.query.comm;
    let annee = req.query.annee === undefined ? null : req.query.annee;
    let mois = req.query.mois === undefined ? null : req.query.mois;
    kpiService.getTauxConversion(comm, annee, mois)
        .then(kpi => res.json(kpi))
        .catch(next);
}

function getPipeline(req, res, next) {
    let comm = req.query.comm === undefined ? null : req.query.comm;
    let annee = req.query.annee === undefined ? null : req.query.annee;
    kpiService.getPipeline(comm, annee)
        .then(kpi => res.json(kpi))
        .catch(next);
}

function getByMotif(req, res, next) {
    let comm = req.query.comm === undefined ? null : req.query.comm;
    let annee = req.query.annee === undefined ? null : req.query.annee;
    kpiService.getByMotif(comm, annee)
        .then(kpi => res.json(kpi))
        .catch(next);
}

function getCommerciaux(req, res, next) {
    kpiService.getCommerciaux()
        .then(comm => res.json(comm))
        .catch(next);
}

