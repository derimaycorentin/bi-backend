
const { Sequelize } = require('sequelize');

const { dbName, dbConfig } = require('config.json');

module.exports = db = { };

getDB();

async function getDB() {

    const dialect = 'mssql';
    const host = dbConfig.server;
    const { userName, password } = dbConfig.authentication.options;

    // Connect to db
    db.sequelize = new Sequelize(dbName, userName, password, { host, dialect });
}
