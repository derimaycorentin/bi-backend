
ALTER FUNCTION fnxCheckOffreConversion (
     @IDDocument INT = NULL
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @Result INT = 0

    SELECT
		  @Result = OC.ID_Document
	FROM  Cleanbase.dbo.Offre_Commande OC
	WHERE OC.ID_Document_Origine = @IDDocument

	IF ISNULL(@Result,0)>0
		RETURN 1

	RETURN 0

END