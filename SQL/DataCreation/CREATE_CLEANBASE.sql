

/*	Cr�ation de la base de donn�es	*/
CREATE DATABASE Cleanbase
GO

USE Cleanbase
GO

/*  SCRIPTS DE CREATION DE TABLES  */

CREATE TABLE [Client] (
	[Num_Enreg] INT IDENTITY,
	[ID] INT,
	[Code] INT,
	[Ville] VARCHAR(1000),
	[CodePostal] VARCHAR(1000),
	[Pays] VARCHAR(100),
	[Groupe] INT,
	[NomGroupe] VARCHAR(1000),
	[NumCom] INT,
	[BlocageCommande] BIT,
	[Langue] VARCHAR(100),
	[ZoneTransport] VARCHAR(1000),
	[ModePaiement] VARCHAR(100),
	[ZoneDistribution] VARCHAR(1000),
	[ClientProspect] BIT,

	CONSTRAINT PK_Client PRIMARY KEY (Num_Enreg)
)

CREATE TABLE [Commerciaux] (
	[ID] INT IDENTITY,
	[ID_Commercial] INT,
	[Matricule] INT,
	[NumCom] VARCHAR(100),
	[Nom] VARCHAR(1000),
	[Objectif] INT,

	CONSTRAINT PK_Commerciaux PRIMARY KEY (ID)
)

CREATE TABLE [Type_DC] (
	[ID] INT IDENTITY,
	[Code_Type] CHAR(1),
	[Nom] VARCHAR(1000),
	[Positif] BIT,

	CONSTRAINT PK_Type_DC PRIMARY KEY (ID)
)

CREATE TABLE [Offre_Commande] (
	[ID] INT IDENTITY,
	[ID_Document] INT,
	[ID_Type] INT,
	[Code_Client] INT,
	[Num_Commercial] INT,
	[Date_Crea] VARCHAR(10),
	[Date_Offre] VARCHAR(10),
	[Date_Doc] VARCHAR(10),
	[Valeur_Nette] DECIMAL(9,2),
	[Date_Livraison] VARCHAR(10),
	[Date_Commande] VARCHAR(10),
	[Motif] VARCHAR(100),
	[ID_Document_Origine] INT,
	[Taux_Reussite] INT,
	[Date_Garantie] VARCHAR(10),

	CONSTRAINT PK_Offre_Commande PRIMARY KEY (ID)
)

GO

INSERT INTO Type_DC (Code_Type, Nom, Positif) VALUES ('B','Offre',1)
INSERT INTO Type_DC (Code_Type, Nom, Positif) VALUES ('C','Commande',1)
INSERT INTO Type_DC (Code_Type, Nom, Positif) VALUES ('H','SAV',0)
INSERT INTO Type_DC (Code_Type, Nom, Positif) VALUES ('K','SAV',0)
INSERT INTO Type_DC (Code_Type, Nom, Positif) VALUES ('I','SAV',0)

