

USE Starbase
GO

-- EXEC spiKPI_Motif_Offre

IF OBJECT_ID('spiKPI_Motif_Offre','P') IS NOT NULL
	DROP PROC spiKPI_Motif_Offre
GO

CREATE PROC spiKPI_Motif_Offre
AS
BEGIN
	
	TRUNCATE TABLE KPI_Motif_Offre

	-- On pr�rempli les couples date/commercial
	SELECT DISTINCT
		 D.IDDate
		,CO.IDCommercial
		,NULL AS NbOffreCde
		,NULL AS NbOffreSS
		,NULL AS NbOffrePerdu
		,NULL AS NbOffreCours
		,CAST(0 AS DECIMAL(14,2)) AS Montant
	INTO #TMP
	FROM Cleanbase.dbo.Commerciaux C
	INNER JOIN Commercial CO ON CO.NomCommercial = C.Nom
	INNER JOIN Cleanbase.dbo.Offre_Commande OC ON OC.Num_Commercial = C.NumCom
	INNER JOIN [Date] D ON D.Mois = CAST(RIGHT(LEFT(OC.Date_Doc,5),2) AS INT) AND D.Ann�e = CAST(RIGHT(OC.Date_Doc,4) AS INT)
	WHERE CO.IDCommercial <> 1

	/**		Devis : Command�	**/
	UPDATE T
	SET T.NbOffreCde = ISNULL(O.NbOffreCde,0)
	FROM #TMP T
	INNER JOIN (
		SELECT
			 CAST(RIGHT(LEFT(OO.Date_Doc,5),2) AS INT) AS Mois
			,CAST(RIGHT(OO.Date_Doc,4) AS INT) AS Ann�e
			,CO.IDCommercial
			,COUNT(*) AS NbOffreCde
		FROM Cleanbase.dbo.Offre_Commande OO
		INNER JOIN Cleanbase.dbo.Commerciaux C ON C.NumCom = OO.Num_Commercial
		INNER JOIN Commercial CO ON CO.NomCommercial = C.Nom
		WHERE OO.ID_Type = 1
		AND (Motif = 'Devis : Command�' OR ID_Document_Origine IS NOT NULL)
		GROUP BY CAST(RIGHT(LEFT(OO.Date_Doc,5),2) AS INT), CAST(RIGHT(OO.Date_Doc,4) AS INT), CO.IDCommercial, Motif
	) O ON O.IDCommercial = T.IDCommercial
	INNER JOIN [Date] D ON D.Ann�e = O.Ann�e AND D.Mois = O.Mois AND D.IDDate = T.IDDate

	/**		Devis : Sans suite	**/
	UPDATE T
	SET T.NbOffreSS = ISNULL(O.NbOffreSS,0)
	FROM #TMP T
	INNER JOIN (
		SELECT
			 CAST(RIGHT(LEFT(OO.Date_Doc,5),2) AS INT) AS Mois
			,CAST(RIGHT(OO.Date_Doc,4) AS INT) AS Ann�e
			,CO.IDCommercial
			,COUNT(*) AS NbOffreSS
		FROM Cleanbase.dbo.Offre_Commande OO
		INNER JOIN Cleanbase.dbo.Commerciaux C ON C.NumCom = OO.Num_Commercial
		INNER JOIN Commercial CO ON CO.NomCommercial = C.Nom
		WHERE OO.ID_Type = 1
		AND Motif = 'Devis : Sans suite'
		GROUP BY CAST(RIGHT(LEFT(OO.Date_Doc,5),2) AS INT), CAST(RIGHT(OO.Date_Doc,4) AS INT), CO.IDCommercial, Motif
	) O ON O.IDCommercial = T.IDCommercial
	INNER JOIN [Date] D ON D.Ann�e = O.Ann�e AND D.Mois = O.Mois AND D.IDDate = T.IDDate


	/**		Devis : Perdu	**/
	UPDATE T
	SET T.NbOffrePerdu = ISNULL(O.NbOffrePerdu,0)
	FROM #TMP T
	INNER JOIN (
		SELECT
			 CAST(RIGHT(LEFT(OO.Date_Doc,5),2) AS INT) AS Mois
			,CAST(RIGHT(OO.Date_Doc,4) AS INT) AS Ann�e
			,CO.IDCommercial
			,COUNT(*) AS NbOffrePerdu
		FROM Cleanbase.dbo.Offre_Commande OO
		INNER JOIN Cleanbase.dbo.Commerciaux C ON C.NumCom = OO.Num_Commercial
		INNER JOIN Commercial CO ON CO.NomCommercial = C.Nom
		WHERE OO.ID_Type = 1
		AND Motif = 'Devis : Perdu'
		GROUP BY CAST(RIGHT(LEFT(OO.Date_Doc,5),2) AS INT), CAST(RIGHT(OO.Date_Doc,4) AS INT), CO.IDCommercial, Motif
	) O ON O.IDCommercial = T.IDCommercial
	INNER JOIN [Date] D ON D.Ann�e = O.Ann�e AND D.Mois = O.Mois AND D.IDDate = T.IDDate


	/**		Devis : En cours	**/
	UPDATE T
	SET  T.NbOffreCours = ISNULL(O.NbOffreCours,0)
		,T.Montant = T.Montant + O.Montant
	FROM #TMP T
	INNER JOIN (
		SELECT
			 CAST(RIGHT(LEFT(OO.Date_Doc,5),2) AS INT) AS Mois
			,CAST(RIGHT(OO.Date_Doc,4) AS INT) AS Ann�e
			,CO.IDCommercial
			,COUNT(*) AS NbOffreCours
			,SUM(Valeur_Nette) AS Montant
		FROM Cleanbase.dbo.Offre_Commande OO
		INNER JOIN Cleanbase.dbo.Commerciaux C ON C.NumCom = OO.Num_Commercial
		INNER JOIN Commercial CO ON CO.NomCommercial = C.Nom
		WHERE OO.ID_Type = 1
		AND Motif = 'Devis : En cours'
		GROUP BY CAST(RIGHT(LEFT(OO.Date_Doc,5),2) AS INT), CAST(RIGHT(OO.Date_Doc,4) AS INT), CO.IDCommercial, Motif
	) O ON O.IDCommercial = T.IDCommercial
	INNER JOIN [Date] D ON D.Ann�e = O.Ann�e AND D.Mois = O.Mois AND D.IDDate = T.IDDate

	
	-- Insertion finale
	INSERT INTO KPI_Motif_Offre (IDDate, IDCommercial, NbOffreCde, NbOffreSS, NbOffrePerdu, NbOffreCours, Montant)
	SELECT DISTINCT
		 T.IDDate
		,T.IDCommercial
		,T.NbOffreCde
		,T.NbOffreSS
		,T.NbOffrePerdu
		,T.NbOffreCours
		,T.Montant
	FROM #TMP T

END