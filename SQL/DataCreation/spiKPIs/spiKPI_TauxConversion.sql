

USE Starbase
GO

-- EXEC spiKPI_TauxConversion

IF OBJECT_ID('spiKPI_TauxConversion','P') IS NOT NULL
	DROP PROC spiKPI_TauxConversion
GO

CREATE PROC spiKPI_TauxConversion
AS
BEGIN
	
	TRUNCATE TABLE KPI_TauxConversion

	-- On pr�rempli les couples commercial/date
	SELECT DISTINCT
		  D.IDDate
		 ,CO.IDCommercial
		 ,NULL AS NombreOffre
		 ,NULL AS NombreCommande
	INTO  #TMP
	FROM  Cleanbase.dbo.Commerciaux C
		  INNER JOIN Commercial CO ON CO.NomCommercial = C.Nom
		  INNER JOIN Cleanbase.dbo.Offre_Commande OC ON OC.Num_Commercial = C.NumCom
		  INNER JOIN [Date] D ON D.Mois = CAST(RIGHT(LEFT(OC.Date_Doc,5),2) AS INT) AND D.Ann�e = CAST(RIGHT(OC.Date_Doc,4) AS INT)
	WHERE CO.IDCommercial <> 1


	-- R�cup�ration du nombre d'offres pour chaque couple
	UPDATE T
	SET  T.NombreOffre = OO.NombreOffre
	FROM #TMP T
	INNER JOIN (
		 SELECT
				 CAST(RIGHT(LEFT(OO.Date_Doc,5),2) AS INT) AS Mois
			    ,CAST(RIGHT(OO.Date_Doc,4) AS INT) AS Ann�e
			    ,CO.IDCommercial
			    ,COUNT(*) AS NombreOffre
		 FROM	 Cleanbase.dbo.Offre_Commande OO
				 INNER JOIN Cleanbase.dbo.Commerciaux C ON C.NumCom = OO.Num_Commercial
				 INNER JOIN Commercial CO ON CO.NomCommercial = C.Nom
		 WHERE	 OO.ID_Type = 1
				 AND CO.IDCommercial <> 1
		GROUP BY CAST(RIGHT(LEFT(OO.Date_Doc,5),2) AS INT), CAST(RIGHT(OO.Date_Doc,4) AS INT), CO.IDCommercial
	) OO ON OO.IDCommercial = T.IDCommercial
	INNER JOIN [Date] D ON D.Ann�e = OO.Ann�e AND D.Mois = OO.Mois AND D.IDDate = T.IDDate


	-- R�cup�ration du nombre de commandes pour chaque couple
	UPDATE T
	SET  T.NombreCommande = OC.NombreCommande
	FROM #TMP T
	INNER JOIN (
		SELECT
				 CAST(RIGHT(LEFT(OC.Date_Doc,5),2) AS INT) AS Mois
				,CAST(RIGHT(OC.Date_Doc,4) AS INT) AS Ann�e
				,CO.IDCommercial
				,COUNT(*) AS NombreCommande
		FROM	 Cleanbase.dbo.Offre_Commande OC
				 INNER JOIN Cleanbase.dbo.Commerciaux C ON C.NumCom = OC.Num_Commercial
				 INNER JOIN Commercial CO ON CO.NomCommercial = C.Nom
		WHERE	 OC.ID_Type = 2
				 AND CO.IDCommercial <> 1
		GROUP BY CAST(RIGHT(LEFT(OC.Date_Doc,5),2) AS INT), CAST(RIGHT(OC.Date_Doc,4) AS INT), CO.IDCommercial
	) OC ON OC.IDCommercial = T.IDCommercial
	INNER JOIN [Date] D ON D.Ann�e = OC.Ann�e AND D.Mois = OC.Mois AND D.IDDate = T.IDDate


	-- Insertion finale
	INSERT INTO KPI_TauxConversion (IDDate, IDCommercial, NombreOffre, NombreCommande)
	SELECT DISTINCT
		 T.IDDate
		,T.IDCommercial
		,T.NombreOffre
		,T.NombreCommande
	FROM #TMP T

END