
USE Starbase
GO

-- EXEC spiKPI_CA_Date
-- select * from Date

IF OBJECT_ID('spiKPI_CA_Date','P') IS NOT NULL
	DROP PROC spiKPI_CA_Date
GO

CREATE PROC spiKPI_CA_Date
AS
BEGIN
	
	TRUNCATE TABLE KPI_CA_Date

	INSERT INTO KPI_CA_Date (IDDate, IDCommercial, Montant)
	SELECT
			 D.IDDate
			,CO.IDCommercial
			,SUM(OC.Valeur_Nette) AS 'Montant'
	FROM	 Cleanbase.dbo.Offre_Commande OC
			 INNER JOIN Cleanbase.dbo.Commerciaux C ON OC.Num_Commercial = C.NumCom
			 INNER JOIN Commercial CO ON C.Nom = CO.NomCommercial
			 INNER JOIN [Date] D ON D.Mois = CAST(RIGHT(LEFT(OC.Date_Doc,5),2) AS INT) AND D.Ann�e = CAST(RIGHT(OC.Date_Doc,4) AS INT)
	WHERE	 OC.ID_Type = 2
			 AND CO.IDCommercial <> 1
	GROUP BY D.IDDate, CO.IDCommercial

END

