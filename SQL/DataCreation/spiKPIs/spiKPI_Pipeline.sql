
USE Starbase
GO

-- EXEC spiKPI_Pipeline
-- select * from KPI_Pipeline

IF OBJECT_ID('spiKPI_Pipeline','P') IS NOT NULL
	DROP PROC spiKPI_Pipeline
GO

CREATE PROC spiKPI_Pipeline
AS
BEGIN
	
	TRUNCATE TABLE KPI_Pipeline

	INSERT INTO KPI_Pipeline (IDDate, IDCommercial, TauxReussite, Montant)
	SELECT
			 D.IDDate
			,CO.IDCommercial
			,OC.Taux_Reussite
			,SUM(OC.Valeur_Nette) AS 'Montant'
	FROM	 Cleanbase.dbo.Offre_Commande OC
			 INNER JOIN Cleanbase.dbo.Commerciaux C ON OC.Num_Commercial = C.NumCom
			 INNER JOIN Cleanbase.dbo.Type_DC T ON T.ID = OC.ID_Type
			 INNER JOIN Commercial CO ON C.Nom = CO.NomCommercial
			 INNER JOIN [Date] D ON D.Mois = CAST(RIGHT(LEFT(OC.Date_Doc,5),2) AS INT) AND D.Ann�e = CAST(RIGHT(OC.Date_Doc,4) AS INT)
	WHERE	 T.ID = 1
			 AND CO.IDCommercial <> 1
			 AND dbo.fnxCheckOffreConversion(OC.ID_Document) = 0
	GROUP BY D.IDDate, CO.IDCommercial, OC.Taux_Reussite

END
