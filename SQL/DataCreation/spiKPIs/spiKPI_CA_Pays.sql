
USE Starbase
GO

-- EXEC spiKPI_CA_Pays
-- select * from KPI_CA_Pays

IF OBJECT_ID('spiKPI_CA_Pays','P') IS NOT NULL
	DROP PROC spiKPI_CA_Pays
GO

CREATE PROC spiKPI_CA_Pays
AS
BEGIN
	
	TRUNCATE TABLE KPI_CA_Pays

	INSERT INTO KPI_CA_Pays (IDPays, IDCommercial, IDDate, Montant)
	SELECT
			 P.IDPays
			,CO.IDCommercial
			,D.IDDate
			,SUM(OC.Valeur_Nette) AS 'Montant'
	FROM	 Cleanbase.dbo.Offre_Commande OC
			 INNER JOIN Cleanbase.dbo.Commerciaux C ON OC.Num_Commercial = C.NumCom
			 INNER JOIN Cleanbase.dbo.Client CL ON OC.Code_Client = CL.Code
	 		 INNER JOIN Pays P ON P.Code = CL.Pays
			 INNER JOIN Commercial CO ON C.Nom = CO.NomCommercial
			 INNER JOIN [Date] D ON D.Ann�e = CAST(RIGHT(OC.Date_Doc,4) AS INT)
	WHERE	 OC.ID_Type = 2
			 AND CO.IDCommercial <> 1
	GROUP BY P.IDPays, CO.IDCommercial, D.IDDate

END
