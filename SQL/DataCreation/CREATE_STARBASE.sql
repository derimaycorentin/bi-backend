

/*	Cr�ation de la base de donn�es	*/
CREATE DATABASE Starbase
GO

USE Starbase
GO

/*  CREATION DES TABLES DE DIMENSION  */

CREATE TABLE [Pays] (
	[IDPays] INT IDENTITY,
	[NomPays] VARCHAR(200),
	[Code] CHAR(2),
	[EstFrance] BIT,
	[NbClient] INT,

	CONSTRAINT PK_Pays PRIMARY KEY (IDPays)
)

CREATE TABLE [Date] (
	[IDDate] INT IDENTITY,
	[Mois] INT,
	[Ann�e] INT,

	CONSTRAINT PK_Date PRIMARY KEY (IDDate)
)

CREATE TABLE [Commercial] (
	[IDCommercial] INT IDENTITY,
	[NomCommercial] VARCHAR(100),
	[Objectif] INT,

	CONSTRAINT PK_Commercial PRIMARY KEY (IDCommercial)
)



/*  CREATION DES TABLES DE KPI  */

CREATE TABLE [KPI_CA_Pays] (
	[IDCAPays] INT IDENTITY,
	[IDPays] INT,
	[IDCommercial] INT,
	[IDDate] INT,
	[Montant] DECIMAL(12,2),

	CONSTRAINT PK_KPI_CA_Pays PRIMARY KEY (IDCAPays)
)

CREATE TABLE [KPI_TauxConversion] (
	[IDTauxConversion] INT IDENTITY,
	[IDDate] INT,
	[IDCommercial] INT,
	[TauxReussite] INT,
	[NombreOffre] INT,
	[NombreCommande] INT,

	CONSTRAINT PK_KPI_TauxConversion PRIMARY KEY (IDTauxConversion)
)

CREATE TABLE [KPI_CA_Date] (
	[IDCADate] INT IDENTITY,
	[IDDate] INT,
	[IDCommercial] INT,
	[Montant] DECIMAL(12,2),

	CONSTRAINT PK_KPI_CA_Date PRIMARY KEY (IDCADate)
)

CREATE TABLE [KPI_Pipeline] (
	[IDPipeline] INT IDENTITY,
	[IDDate] INT,
	[IDCommercial] INT,
	[TauxReussite] DECIMAL(12,2),
	[Montant] DECIMAL(12,2),

	CONSTRAINT PK_KPI_Pipeline PRIMARY KEY (IDPipeline)
)

CREATE TABLE KPI_Motif_Offre (
	IDMotifOffre INT IDENTITY,
	IDDate INT,
	[IDCommercial] INT,
	NbOffreCde INT,
	NbOffreSS INT,
	NbOffrePerdu INT,
	NbOffreCours INT,
	[Montant] DECIMAL(14,2),

	CONSTRAINT PK_KPI_Motif_Offre PRIMARY KEY (IDMotifOffre)
)
