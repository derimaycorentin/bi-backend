

USE Starbase
GO

-- EXEC spiCommerciaux
-- select * from Commercial

IF OBJECT_ID('spiCommerciaux','P') IS NOT NULL
	DROP PROC spiCommerciaux
GO

CREATE PROC spiCommerciaux
AS
BEGIN

	TRUNCATE TABLE Commercial
	
	INSERT INTO Commercial (NomCommercial, Objectif)
	SELECT
		 Nom
		,Objectif
	FROM Cleanbase.dbo.Commerciaux
	WHERE Objectif <> 0

END