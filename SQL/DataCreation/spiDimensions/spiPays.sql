

USE Starbase
GO

-- EXEC spiPays
-- select * from Pays

IF OBJECT_ID('spiPays','P') IS NOT NULL
	DROP PROC spiPays
GO

CREATE PROC spiPays
AS
BEGIN

	TRUNCATE TABLE Pays
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('AD','Andorre',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('AE','�mirats Arabes Unis',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('AR','Argentine',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('AT','Autriche',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('AU','Australie	',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('BE','Belgium',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('BG','Bulgaria',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('BR','Br�sil',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('BY','Bi�lorussie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('CA','Canada',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('CG','R�publique du Congo',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('CH','Suisse',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('CI','C�te d�Ivoire',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('CL','Chili',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('CM','Cameroun',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('CN','Chine',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('CO','Colombie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('CY','Chypre',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('CZ','R�publique tch�que',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('DE','Allemagne',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('DK','Danemark',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('DO','R�publique dominicaine',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('DZ','Alg�rie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('EE','Estonie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('EG','�gypte',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('ES','Espagne',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('FR','France',1)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('GA','Gabon',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('GB','Royaume-Uni',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('GH','Ghana',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('GN','Guin�e',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('GP','Guadeloupe',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('GR','Gr�ce',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('HK','Hong Kong',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('HT','Ha�ti',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('HU','Hongrie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('ID','Indon�sie	',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('IE','Irlande',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('IL','Isra�l',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('IN','Inde',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('IR','Iran',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('IT','Italie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('JO','Jordanie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('JP','Japon',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('KE','Kenya',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('KR','Cor�e du Sud',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('KW','Kowe�t',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('KZ','Kazakhstan',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('LB','Liban',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('LK','Sri Lanka',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('LT','Lituanie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('LU','Luxembourg',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('LV','Lettonie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('LY','Libye',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('MA','Maroc',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('MC','Monaco',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('MD','Moldavie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('ML','Mali',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('MQ','Martinique',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('MR','Mauritanie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('MU','Maurice',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('MX','Mexique',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('MY','Malaisie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('MZ','Mozambique',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('NC','Nouvelle-Cal�donie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('NG','Nigeria',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('NL','Pays-Bas',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('NO','Norv�ge',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('NZ','Nouvelle-Z�lande',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('OM','Oman',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('PA','Panama',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('PG','Papouasie-Nouvelle-Guin�e',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('PH','Philippines',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('PK','Pakistan',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('PL','Pologne',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('PT','Portugal',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('QA','Qatar',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('RE','R�union',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('RO','Roumanie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('RS','Serbie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('RU','Russie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('SA','Arabie Saoudite',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('SE','Su�de',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('SG','Singapour',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('SI','Slov�nie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('SK','Slovaquie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('SN','S�n�gal',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('SR','Suriname',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('SY','Syrie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('TG','Togo',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('TH','Tha�lande',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('TN','Tunisie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('TR','Turquie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('TW','Taiwan',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('TZ','Tanzanie',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('UA','Ukraine',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('US','�tats-Unis',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('YE','Y�men',0)
	INSERT INTO Pays (Code,NomPays,EstFrance) VALUES ('ZA','Afrique du Sud',0)

	UPDATE P
	SET  P.NbClient = A.NbPays
	FROM Pays P
	INNER JOIN (
		SELECT
					COUNT(Pays) AS 'NbPays'
				,Pays
		FROM	 Cleanbase.dbo.Client C
		GROUP BY Pays
	) A ON A.Pays = P.Code

END