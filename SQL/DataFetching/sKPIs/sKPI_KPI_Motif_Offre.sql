USE Starbase
GO

-- EXEC sKPI_Motif_Offre @IDCommercial = 2

IF OBJECT_ID('sKPI_Motif_Offre','P') IS NOT NULL
    DROP PROC sKPI_Motif_Offre
GO

CREATE PROC sKPI_Motif_Offre (
     @Ann�e INT = NULL
    ,@IDCommercial INT = NULL
)
AS
BEGIN

    SELECT
			 D.Ann�e
			,C.NomCommercial
			,ISNULL(SUM(K.NbOffreCde),0) AS NbOffreCde
			,ISNULL(SUM(K.NbOffreSS),0) AS NbOffreSS
			,ISNULL(SUM(K.NbOffrePerdu),0) AS NbOffrePerdu
			,ISNULL(SUM(K.NbOffreCours),0) AS NbOffreCours
			,ISNULL(CAST(CAST(SUM(K.NbOffreCde) AS DECIMAL(4,2)) * 100 / (ISNULL(SUM(K.NbOffreCde),0)
																		+ ISNULL(SUM(K.NbOffreSS),0)
																		+ ISNULL(SUM(K.NbOffrePerdu),0)
																		+ ISNULL(SUM(K.NbOffreCours),0)) AS DECIMAL(4,2)),0) AS Ratio
			,SUM(K.Montant) AS Montant
    FROM	 KPI_Motif_Offre K
			 INNER JOIN [Date] D ON D.IDDate = K.IDDate
			 INNER JOIN Commercial C ON C.IDCommercial = K.IDCommercial
    WHERE	 (@Ann�e IS NULL OR (D.Ann�e = @Ann�e))
			 AND (@IDCommercial IS NULL OR (K.IDCommercial = @IDCommercial))
	GROUP BY D.Ann�e, C.NomCommercial

END