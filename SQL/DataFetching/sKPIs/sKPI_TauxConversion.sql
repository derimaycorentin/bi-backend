USE Starbase
GO

-- EXEC sKPI_TauxConversion

IF OBJECT_ID('sKPI_TauxConversion','P') IS NOT NULL
    DROP PROC sKPI_TauxConversion
GO

CREATE PROC sKPI_TauxConversion (
     @Ann�e INT = NULL
	,@Mois INT = NULL
    ,@IDCommercial INT = NULL
)
AS
BEGIN

    SELECT
			D.Ann�e
		   ,dbo.fnxParsingMonth(D.Mois) AS 'Mois'
		   ,C.NomCommercial
		   ,K.NombreCommande
		   ,K.NombreOffre
    FROM	KPI_TauxConversion K
			INNER JOIN [Date] D ON D.IDDate = K.IDDate
			INNER JOIN Commercial C ON C.IDCommercial = K.IDCommercial
    WHERE	(@Ann�e IS NULL OR (D.Ann�e = @Ann�e))
			AND   (@Mois IS NULL OR (D.Mois = @Mois))
			AND   (@IDCommercial IS NULL OR (K.IDCommercial = @IDCommercial))

END