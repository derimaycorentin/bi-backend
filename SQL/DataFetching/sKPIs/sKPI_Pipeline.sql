USE Starbase
GO

-- EXEC sKPI_Pipeline @Annee = 2016
-- select * from KPI_Pipeline

IF OBJECT_ID('sKPI_Pipeline','P') IS NOT NULL
    DROP PROC sKPI_Pipeline
GO

CREATE PROC sKPI_Pipeline (
     @Annee INT = NULL
    ,@IDCommercial INT = NULL
)
AS
BEGIN

	-- Requ�te dynamique String
	DECLARE @Query NVARCHAR(2000)

	-- 4 variable pour la consommation de chaque tier du pipeline
	DECLARE @Tier1 INT
	DECLARE @Tier2 INT
	DECLARE @Tier3 INT
	DECLARE @Tier4 INT

	-- Patron de la requ�te, avec des flag (TIER_NUMBER, TIER_PORTION) � remplacer dynamiquement
	SET @Query = '
		SELECT DISTINCT
			@TierTIER_NUMBER = SUM(K.Montant)
		FROM [Date] D
		LEFT JOIN KPI_Pipeline K ON D.IDDate = K.IDDate AND K.TauxReussite TIER_PORTION
		WHERE 1=1 ' 
	
	-- Filtre/condition
	IF (@IDCommercial IS NOT NULL)
		SET @Query += ' AND K.IDCommercial = ' + CAST(@IDCommercial AS VARCHAR(10))
	IF (@Annee IS NOT NULL)
		SET @Query += ' AND D.Ann�e = ' + CAST(@Annee AS VARCHAR(10))

	-- Construction dynamique des 4 requ�tes, pour chaque tranche de consommation
	DECLARE @Query1 NVARCHAR(2000) = REPLACE(REPLACE(@Query,'TIER_NUMBER','1'),'TIER_PORTION','BETWEEN 0 AND 30')
	EXEC sp_executesql @Query1, N'@Tier1 INT OUT', @Tier1 OUT 

	DECLARE @Query2 NVARCHAR(2000) = REPLACE(REPLACE(@Query,'TIER_NUMBER','2'),'TIER_PORTION','BETWEEN 31 AND 60')
	EXEC sp_executesql @Query2, N'@Tier2 INT OUT', @Tier2 OUT 

	DECLARE @Query3 NVARCHAR(2000) = REPLACE(REPLACE(@Query,'TIER_NUMBER','3'),'TIER_PORTION','BETWEEN 61 AND 80')
	EXEC sp_executesql @Query3, N'@Tier3 INT OUT', @Tier3 OUT 

	DECLARE @Query4 NVARCHAR(2000) = REPLACE(REPLACE(@Query,'TIER_NUMBER','4'),'TIER_PORTION','> 81')
	EXEC sp_executesql @Query4, N'@Tier4 INT OUT', @Tier4 OUT 


    SELECT DISTINCT
          @Tier1 AS '0-30%'
		 ,@Tier2 AS '31-60%'
		 ,@Tier3 AS '61-80%'
		 ,@Tier4 AS '>81%'

END