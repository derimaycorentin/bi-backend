
USE Starbase
GO

-- EXEC sKPI_CA_Pays @EstFrance=1, @IDCommercial = 6, @Année=2016

IF OBJECT_ID('sKPI_CA_Pays','P') IS NOT NULL
	DROP PROC sKPI_CA_Pays
GO

CREATE PROC sKPI_CA_Pays (
	 @CodePays CHAR(2) = NULL
	,@IDCommercial INT = NULL
	,@Année INT = NULL
	,@EstFrance BIT = NULL
)
AS
BEGIN

	DECLARE @Query NVARCHAR(2000)

	-- Sélection des champs principaux
	SET @Query = '
		SELECT DISTINCT
			 P.Code
			,P.NomPays
			,P.NbClient
			,D.Année
			,SUM(K.Montant) AS Montant
		FROM KPI_CA_Pays K
		INNER JOIN Pays P ON P.IDPays = K.IDPays
		INNER JOIN Commercial C ON C.IDCommercial = K.IDCommercial
		INNER JOIN [Date] D ON D.IDDate = K.IDDate
		WHERE 1=1 '
	
	-- Conditions/Filtres
	IF (@CodePays IS NOT NULL)
		SET @Query += ' AND P.Code = ' + CAST(@CodePays AS VARCHAR(10))

	IF (@IDCommercial IS NOT NULL)
		SET @Query += ' AND K.IDCommercial = ' + CAST(@IDCommercial AS VARCHAR(10))

	IF (@EstFrance IS NOT NULL)
		SET @Query += ' AND P.EstFrance = ' + CAST(@EstFrance AS VARCHAR(10))

	IF (@Année IS NOT NULL)
		SET @Query += ' AND D.Année = ' + CAST(@Année AS VARCHAR(10))
		
	-- Groupement des résultats
	SET @Query += ' GROUP BY P.Code, P.NomPays, P.NbClient, D.Année, C.NomCommercial, P.EstFrance, D.IDDate'


	-- Récupération des résultats
	DECLARE @Table TABLE (Code CHAR(2), NomPays VARCHAR(50), NbClient INT, Année INT, Montant DECIMAL(12,2))
	INSERT INTO @Table 
	EXEC (@Query)
	
	-- Select final
	SELECT DISTINCT 
			  Code
			 ,NomPays
			 ,NbClient
			 ,Année
			 ,SUM(Montant) AS Montant 
	FROM	  @Table 
	GROUP BY  Code, NomPays, NbClient, Année

END
