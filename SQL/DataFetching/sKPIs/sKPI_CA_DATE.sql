USE Starbase
GO

-- EXEC sKPI_CA_PAYS

IF OBJECT_ID('sKPI_CA_DATE','P') IS NOT NULL
    DROP PROC sKPI_CA_DATE
GO

CREATE PROC sKPI_CA_DATE (
     @Ann�e INT = NULL
	,@Mois INT = NULL
    ,@IDCommercial INT = NULL
)
AS
BEGIN

    SELECT
			 D.Ann�e
			,D.Mois
			,C.NomCommercial
			,K.Montant
    FROM	 KPI_CA_Date K
			 INNER JOIN Date D ON D.IDDate = K.IDDate
			 INNER JOIN Commercial C ON C.IDCommercial = K.IDCommercial
    WHERE	 (@Ann�e IS NULL OR (D.Ann�e = @Ann�e))
			 AND (@Mois IS NULL OR (D.Mois = @Mois))
			 AND (@IDCommercial IS NULL OR (K.IDCommercial = @IDCommercial))
	ORDER BY D.Mois

END