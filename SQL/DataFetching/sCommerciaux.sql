
USE Starbase
GO

-- EXEC sCommerciaux

IF OBJECT_ID('sCommerciaux','P') IS NOT NULL
	DROP PROC sCommerciaux
GO

CREATE PROC sCommerciaux
AS
BEGIN

	SELECT
		  IDCommercial
		 ,NomCommercial
		 ,Objectif
	FROM  Commercial

END