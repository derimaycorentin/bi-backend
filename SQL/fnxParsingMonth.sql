
CREATE FUNCTION fnxParsingMonth (
     @Mois INT = NULL
)
RETURNS VARCHAR(20)
AS
BEGIN

    IF @Mois = 1
	BEGIN
		RETURN 'Janvier'
	END

	IF @Mois = 2
	BEGIN
		RETURN 'F�vrier'
	END

	IF @Mois = 3
	BEGIN
		RETURN 'Mars'
	END

	IF @Mois = 4
	BEGIN
		RETURN 'Avril'
	END

	IF @Mois = 5
	BEGIN
		RETURN 'Mai'
	END

	IF @Mois = 6
	BEGIN
		RETURN 'Juin'
	END

	IF @Mois = 7
	BEGIN
		RETURN 'Juillet'
	END

	IF @Mois = 8
	BEGIN
		RETURN 'Ao�t'
	END

	IF @Mois = 9
	BEGIN
		RETURN 'Septembre'
	END

	IF @Mois = 10
	BEGIN
		RETURN 'Octobre'
	END

	IF @Mois = 11
	BEGIN
		RETURN 'Novembre'
	END

	IF @Mois = 12
	BEGIN
		RETURN 'D�cembre'
	END

	RETURN ''

END